from asyncio import current_task
import os
import random
import time
from selenium import webdriver
from datetime import datetime
from datetime import timedelta
import pytz

def test_availability(verbose=True):
    '''Returns False if the reservation process returns a forbidden error.'''
    guichet_ids = [990, 1000, 1001, 1002, 1003, 1005, 1028, 1029, 1063]

    guichet_dict = {990: 1,
    1000: 4,
    1001: 6,
    1002: 5,
    1003: 7,
    1005: 8,
    1028: 9,
    1029: 10,
    1063: 11
    }

    browser = webdriver.Firefox()
    browser.get("https://pprdv.interieur.gouv.fr/booking/create/989")
    time.sleep(7)
    try:
        if verbose:
            print("Loading landing page...")

        browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        browser.find_element_by_name("condition").click()

        if verbose:
            print("Loaded.")

        time.sleep(3)
        browser.find_element_by_name("nextButton").click()
        time.sleep(10)
    except BaseException as err:
        browser.get_screenshot_as_file("screenshot.png")
        print(f"Unexpected {err}, {type(err)}")
        print("Failed on landing page.")
        browser.quit()
        return False
    
    guichet_id = random.choice(guichet_ids)
    id_tag = "planning" + str(guichet_id)
    try:
        if verbose:
            print("Loading desk choice page...")
        browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        browser.find_element_by_id(id_tag).click()
        if verbose:
            print("Loaded.")
        time.sleep(3)
        browser.find_element_by_name("nextButton").click()
        time.sleep(5)
    except BaseException as err:
        browser.get_screenshot_as_file("screenshot.png")
        print(f"Unexpected {err}, {type(err)}")
        print("Failed on choice page.")
        browser.quit()
        return False

    try:
        if verbose:
            print("Loading reservation page...")

        if browser.find_element_by_xpath("//*[ contains (text(), 'votre demande de rendez-vous. Veuillez recommencer')]").text == "Il n'existe plus de plage horaire libre pour votre demande de rendez-vous. Veuillez recommencer ultérieurement.":
            print("No reservation at guichet {}.".format(guichet_dict[guichet_id]))
            browser.quit()
            return True
        elif browser.title == "Remise d'un titre de séjour étranger - Les services de l'État Préfecture de Police":
            print("There's an appointment at guichet {}!".format(guichet_dict[guichet_id]))
            browser.quit()
            # return guichet_dict[guichet_id]
            return True
        else:
            print("Something went wrong.")
            browser.quit()
            return False
    except BaseException as err:
        # if browser.title == "Remise d'un titre de séjour étranger - Les services de l'État Préfecture de Police":
        #     # browser.close()
        #     return guichet_dict[guichet_id]
        # else:
        browser.get_screenshot_as_file("screenshot.png")
        print(f"Unexpected {err}, {type(err)}")
        print("Failed on reservation page.")
        browser.quit()
        return False

    # browser.quit()

def loop_availability():
    while True:
        if test_availability():
            continue
        else:
            break

def wait_hours(hours):
    '''Returns current time, the time after n hours rounded _up_, and the difference in seconds.'''
    timezone_paris = pytz.timezone('Europe/Paris')
    current_time = datetime.now(timezone_paris)
    # We'll round down this time.
    next_time_unrounded = current_time + timedelta(hours=hours+1)
    next_time = next_time_unrounded - timedelta(minutes=next_time_unrounded.minute,
                                                seconds = next_time_unrounded.second,
                                                microseconds = next_time_unrounded.microsecond) 
    # return next_time
    return current_time, next_time, (next_time- current_time).total_seconds()




# test availability once. If error, add an extra hour.    


# print(test_availability())
if __name__ == "__main__":
    # loop_availability()
    # print(wait_hours(1)[1].strftime("%B %d, %Y at %H:%M:%S"))
    timezone_paris = pytz.timezone('Europe/Paris')
    while True:
        # If you try your single attempt after one loop and it fails, you should wait two hours. So why tries=1 after it fails?
        # If you start at 7:00 sharp, you'll want to try again at 9:00. Wait_hours rounds up, so you'll achieve this by waiting one hour rounded up.
        tries = 0
        while True:
            print("Starting single attempt...")
            if test_availability():
                break
            else:
                tries += 1
                _, next_time, seconds = wait_hours(tries)
                print("Will retry single attempt at {}.".format(next_time.strftime("%B %d, %Y at %H:%M:%S")))
                time.sleep(seconds)

        print("Starting new loop at {}.".format(datetime.now(timezone_paris).strftime("%B %d, %Y at %H:%M:%S")))
        loop_availability()
        _, next_time, seconds = wait_hours(1)
        print("Will resume at {}.".format(next_time.strftime("%B %d, %Y at %H:%M:%S")))
        print("-------------------------")
        time.sleep(seconds)
